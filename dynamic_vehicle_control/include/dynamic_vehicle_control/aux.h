#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>
using namespace Eigen;

#define PI 3.1415926535 

VectorXd rot2quat(Matrix3d R);
Matrix3d quat2rot(VectorXd e);
Matrix3d rpy2rot(Vector3d rpy);
VectorXd R2axis(Matrix3d R);
VectorXd rpy2quat(Vector3d rpy);
Vector3d quat2rpy(VectorXd quat);
Vector3d rot2rpy_1(Matrix3d R);
double *rot2rpy_2(double **R);
Matrix3d skew(VectorXd v);
void trapezoidal(double qi, double qf, double tf, double t, double &q, double &dq, double &ddq);
VectorXd array1dtoVectorXd(double *m, int size);
int sign(double x);
VectorXd quatError(VectorXd ed, VectorXd e);
