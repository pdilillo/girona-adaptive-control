#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "dynamic_vehicle_control" for configuration ""
set_property(TARGET dynamic_vehicle_control APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(dynamic_vehicle_control PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libdynamic_vehicle_control.so"
  IMPORTED_SONAME_NOCONFIG "libdynamic_vehicle_control.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS dynamic_vehicle_control )
list(APPEND _IMPORT_CHECK_FILES_FOR_dynamic_vehicle_control "${_IMPORT_PREFIX}/lib/libdynamic_vehicle_control.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
