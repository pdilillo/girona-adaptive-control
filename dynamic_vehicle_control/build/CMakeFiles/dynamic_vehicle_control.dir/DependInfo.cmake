# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/paolo/girona_ws/src/dynamic_vehicle_control/src/AdaptiveControlVehicle.cpp" "/home/paolo/girona_ws/src/dynamic_vehicle_control/build/CMakeFiles/dynamic_vehicle_control.dir/src/AdaptiveControlVehicle.cpp.o"
  "/home/paolo/girona_ws/src/dynamic_vehicle_control/src/DynamicController.cpp" "/home/paolo/girona_ws/src/dynamic_vehicle_control/build/CMakeFiles/dynamic_vehicle_control.dir/src/DynamicController.cpp.o"
  "/home/paolo/girona_ws/src/dynamic_vehicle_control/src/aux.cc" "/home/paolo/girona_ws/src/dynamic_vehicle_control/build/CMakeFiles/dynamic_vehicle_control.dir/src/aux.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
