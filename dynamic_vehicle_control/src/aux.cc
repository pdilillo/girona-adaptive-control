#include <dynamic_vehicle_control/aux.h>

using namespace std;

VectorXd rot2quat(Matrix3d R)
{

	/*
        Transformation from rotation matrix to quaternion

        input:

                Eigen::Matrix3d R		dim: 3x3	rotation matrix

        output:

                Eigen::VectorXd e1	dim: 4x1	quaternion


 */

	double *e = new double[4];

	int signum1 = sign(R(2, 1) - R(1, 2));
	int signum2 = sign(R(0, 2) - R(2, 0));
	int signum3 = sign(R(1, 0) - R(0, 1));

	double argument1 = R(0, 0) - R(1, 1) - R(2, 2) + 1.0;
	double argument2 = -R(0, 0) + R(1, 1) - R(2, 2) + 1.0;
	double argument3 = -R(0, 0) - R(1, 1) + R(2, 2) + 1.0;

	if (argument1 < 0.0 && argument1 > -pow(10, -12))
	{
		//     cout<<"WARNING: argument1 in function 'rot2quat' is negative and near zero: -10^(-12) < argument1 < 0"<<endl;
		argument1 = 0.0;
	}
	else if (argument1 < -pow(10, -12))
	{
		//   cout<<"WARNING: argument1 in function 'rot2quat' is negative and NOT near zero: argument1 < -10^(-12)"<<endl;
		argument1 = 0.0;
	}
	if (argument2 < 0.0 && argument2 > -pow(10, -12))
	{
		// cout<<"WARNING: argument2 in function 'rot2quat' is negative and near zero: -10^(-12) < argument2 < 0"<<endl;
		argument2 = 0.0;
	}
	else if (argument2 < -pow(10, -12))
	{
		//cout<<"WARNING: argument2 in function 'rot2quat' is negative and NOT near zero: argument2 < -10^(-12)"<<endl;
		argument2 = 0.0;
	}
	if (argument3 < 0.0 && argument3 > -pow(10, -12))
	{
		//  cout<<"WARNING: argument3 in function 'rot2quat' is negative and near zero: -10^(-12) < argument3 < 0"<<endl;
		argument3 = 0.0;
	}
	else if (argument3 < -pow(10, -12))
	{
		//  cout<<"WARNING: argument3 in function 'rot2quat' is negative and NOT near zero: argument3 < -10^(-12)"<<endl;
		argument3 = 0.0;
	}

	e[3] = 0.5 * sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);

	if (isnan(e[3]))

		e[3] = 0.0;

	if (signum1 >= 0)
		e[0] = 0.5 * sqrt(argument1);
	else
		e[0] = -0.5 * sqrt(argument1);

	if (signum2 >= 0)
		e[1] = 0.5 * sqrt(argument2);
	else
		e[1] = -0.5 * sqrt(argument2);

	if (signum3 >= 0)
		e[2] = 0.5 * sqrt(argument3);
	else
		e[2] = -0.5 * sqrt(argument3);

	//cout <<"\t" << R(0,0) << "\t" << R(1,1) << "\t" << R(2,2) << "\n\n";

	VectorXd e1 = array1dtoVectorXd(e, 4);

	return e1;
}

Matrix3d quat2rot(VectorXd e)
{

	/*
        Transformation from quaternion to rotation matrix

        input:

                Eigen::VectorXd e		dim: 4x1	quaternion

        output:

                Eigen::Matrix3d R		dim: 3x3	rotation matrix


 */

	double t[10];

	Matrix3d R;

	/*
    t[0] = pow(e[0],2);  //x^2
    t[1] = pow(e[1],2);	 //y^2
    t[2] = pow(e[2],2);  //z^2
    t[3] = e[0]*e[1];	 //x*y
    t[4] = e[0]*e[2];	 //x*z
    t[5] = e[1]*e[2];	 //y*z
    t[6] = e[3]*e[0];	 //eta*x
    t[7] = e[3]*e[1];    //eta*y
    t[8] = e[3]*e[2];	 //eta*z


    R(0,0) = 1 - 2 * (t[1] + t[2]);
    R(0,1) = 2 * (t[3] - t[8]);
    R(0,2) = 2 * (t[4] + t[7]);

    R(1,0) = 2 * (t[3] + t[8]);
    R(1,1) = 1 - 2 * (t[0] + t[2]);
    R(1,2) = 2 * (t[5] - t[6]);

    R(2,0) = 2 * (t[4] - t[7]);
    R(2,1) = 2 * (t[5] + t[6]);
    R(2,2) = 1 - 2 * (t[0] + t[1]);  */

	t[0] = pow(e[0], 2); //x^2
	t[1] = pow(e[1], 2); //y^2
	t[2] = pow(e[2], 2); //z^2
	t[3] = pow(e[3], 2); //eta^2
	t[4] = e[0] * e[1];  //x*y
	t[5] = e[0] * e[2];  //x*z
	t[6] = e[1] * e[2];  //y*z
	t[7] = e[3] * e[0];  //eta*x
	t[8] = e[3] * e[1];  //eta*y
	t[9] = e[3] * e[2];  //eta*z

	R(0, 0) = 2 * (t[3] + t[0]) - 1;
	R(0, 1) = 2 * (t[4] - t[9]);
	R(0, 2) = 2 * (t[5] + t[8]);

	R(1, 0) = 2 * (t[4] + t[9]);
	R(1, 1) = 2 * (t[3] + t[1]) - 1;
	R(1, 2) = 2 * (t[6] - t[7]);

	R(2, 0) = 2 * (t[5] - t[8]);
	R(2, 1) = 2 * (t[6] + t[7]);
	R(2, 2) = 2 * (t[3] + t[2]) - 1;

	return R;
}

Matrix3d rpy2rot(Vector3d rpy)
{

	/*
        Transformation from roll-pitch-yaw to rotation matrix

        input:

                Vector3d rpy		dim: 3x1	roll, pitch and yaw angles

        output:

                Matrix3d R		dim: 3x3	rotation matrix


 */

	double phi = rpy[0];
	double theta = rpy[1];
	double psi = rpy[2];

	double cp = cos(psi);
	double ct = cos(theta);
	double cf = cos(phi);

	double sp = sin(psi);
	double st = sin(theta);
	double sf = sin(phi);

	Matrix3d R;

	R(0, 0) = cf * ct;
	R(0, 1) = -sf * cp + cf * st * sp;
	R(0, 2) = sf * sp + cf * st * cp;

	R(1, 0) = sf * ct;
	R(1, 1) = cf * cp + sf * st * sp;
	R(1, 2) = -cf * sp + sf * st * cp;

	R(2, 0) = -st;
	R(2, 1) = ct * sp;
	R(2, 2) = ct * cp;


	R(0, 0) = cp * ct;
	R(0, 1) = -sp * cf + cp * st * sf;
	R(0, 2) = sp * sf + cp * st * cf;

	R(1, 0) = sp * ct;
	R(1, 1) = cp * cf + sp * st * sf;
	R(1, 2) = -cp * sf + sp * st * cf;

	R(2, 0) = -st;
	R(2, 1) = ct * sf;
	R(2, 2) = ct * cf;

	return R;
}

VectorXd rpy2quat(Vector3d rpy)
{

	/*
        Transformation from roll-pitch-yaw to quaternion

        input:

                Vector3d rpy		dim: 3x1	roll, pitch and yaw angles

        output:

                Eigen::VectorXd e	dim: 4x1	quaternion


 */

	Matrix3d R = rpy2rot(rpy);

	VectorXd e = rot2quat(R);

	return e;
}

Vector3d quat2rpy(VectorXd quat)
{

	/*
        Transformation from quaternion ro roll-pitch-yaw

        input:

                double *quat		dim: 4x1	quaternion

        output:

                double *rpy		dim: 3x1	roll, pitch and yaw angles


 */

	Matrix3d R = quat2rot(quat);
	Vector3d rpy = rot2rpy_1(R);

	return rpy;
}

Vector3d rot2rpy_1(Matrix3d R)
{

	/*
        Transformation from rotation matrix ro roll-pitch-yaw with theta in (-pi/2, pi/2)

        input:

                double **R		dim: 3x3	rotation matrix

        output:

                double *rpy		dim: 3x1	roll, pitch and yaw angles


 */

	Vector3d rpy;

	double app = sqrt(pow(R(2, 1), 2) + pow(R(2, 2), 2));

	rpy[2] = atan2(R(1, 0), R(0, 0)); //phi (around z)
	rpy[1] = atan2(-R(2, 0), app);	//theta (around y)
	rpy[0] = atan2(R(2, 1), R(2, 2)); //psi (around x)

	return rpy;
}

double *rot2rpy_2(double **R)
{

	/*
        Transformation from rotation matrix ro roll-pitch-yaw with theta in (pi/2, 3pi/2)

        input:

                double **R		dim: 3x3	rotation matrix

        output:

                double *rpy		dim: 3x1	roll, pitch and yaw angles


 */

	double *rpy = new double[3];

	double app = sqrt(pow(R[2][1], 2) + pow(R[2][2], 2));

	rpy[0] = atan2(-R[1][0], -R[0][0]); //phi (around z)
	rpy[1] = atan2(-R[2][0], -app);		//theta (around y)
	rpy[2] = atan2(-R[2][1], -R[2][2]); //psi (around x)

	return rpy;
}

VectorXd R2axis(Matrix3d R)
{

	VectorXd axis(4);

	axis(0) = acos((R(0, 0) + R(1, 1) + R(2, 2) - 1) / 2);
	axis(1) = 1 / (2 * sin(axis(0))) * (R(2, 1) - R(1, 2));
	axis(2) = 1 / (2 * sin(axis(0))) * (R(0, 2) - R(2, 0));
	axis(3) = 1 / (2 * sin(axis(0))) * (R(1, 0) - R(0, 1));

	return axis;
}

Matrix3d skew(VectorXd v)
{

	Matrix3d skew;

	skew << 0, -v(2), v(1),
		v(2), 0, -v(0),
		-v(1), v(0), 0;

	return skew;
}

void trapezoidal(double qi, double qf, double tf, double t, double &q, double &dq, double &ddq)
{

	/*
        Generates a 1-dimensional trapezoidal velocity profile trajectory

        input:

                double qi		dim: 1		initial value
                double qf		dim: 1		final value
                double tf		dim: 1		final time of the trajectory
                double t		dim: 1		current time
                double &q		dim: 1		output target position
                double &dq		dim: 1		output target velocity
                double &ddq		dim: 1		output target acceleration



 */

	double dq_c = 2 * (qf - qi) / (tf * 1.3);

	double tc = (qi - qf + dq_c * tf) / dq_c;
	double ddq_c = pow(dq_c, 2) / (qi - qf + dq_c * tf);

	if (t >= 0 && t <= tc)
	{

		q = qi + 0.5 * ddq_c * pow(t, 2);
		dq = ddq_c * t;
		ddq = ddq_c;
	}

	else if (t > tc && t <= tf - tc)
	{

		q = qi + ddq_c * tc * (t - 0.5 * tc);
		dq = ddq_c * tc;
		ddq = 0;
	}

	else if (t > tf - tc && t <= tf)
	{

		q = qf - 0.5 * ddq_c * pow(tf - t, 2);
		dq = -ddq_c * t + ddq_c * tf;
		ddq = -ddq_c;
	}

	if (qi == qf)
	{

		q = qi;
		dq = 0;
		ddq = 0;
	}
}

VectorXd array1dtoVectorXd(double *m, int size)
{

	int i;
	VectorXd a(size);

	for (i = 0; i < size; i++)
	{
		a(i) = m[i];
	}

	delete[] m;

	return a;
}

int sign(double x)
{
	if (x >= 0)
		return 1;
	else
		return -1;
}

VectorXd quatError(VectorXd ed, VectorXd e)
{

	/*
        Quaternion error

        input:

                Eigen::VectorXd ed		dim: 4x1	desired quaternion
                Eigen::VectorXd e		dim: 4x1	current quaternion

        output:

                Eigen::Vector3d eo		dim: 3x1	quaternion error


 */

	int i = 0;
	Vector3d ed_app;
	Vector3d e_app;
	Vector3d eo;

	for (i = 0; i < 3; i++)
	{
		ed_app(i) = ed(i);
		e_app(i) = e(i);
	}

	double etatilde = e(3) * ed(3) + e_app.transpose() * ed_app;
	eo = e(3) * ed_app - ed(3) * e_app + ed_app.cross(e_app);
	double angle = atan2(eo.norm(), etatilde);

	if (angle > PI / 2)
	{

		eo = -eo;
	}

	return eo;
}