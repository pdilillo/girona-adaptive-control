#include "dynamic_vehicle_control/AdaptiveControlVehicle.h"
#include "dynamic_vehicle_control/aux.h"

#include <iostream>

AdaptiveControlVehicle::AdaptiveControlVehicle(ctb::ADPGains adpGains, double sampleTime)
    : DynamicController(sampleTime)
{
    s_.setZero(6);
    R_.setIdentity(3, 3);
    Y_.setZero(6, 9);
    gamma_.setZero(9);
    pose_error_.setZero(6);
    velocity_error_.setZero(6);

    integrale.setZero(6);

    z_.resize(3);
    z_ << 0, 0, 1;
    adpGains_.Kl.resize(6);
    adpGains_.Kg.resize(9);
    adpGains_.Ks.resize(6);
    adpGains_.gamma.setZero(9);
    adpGains_ = adpGains;
    upB_.resize(9);
    lowB_.resize(9);
    upB_ << adpGains.upGB_m, adpGains.up_f, adpGains.up_m;
    lowB_ << adpGains.lowGB_m, adpGains.low_f, adpGains.low_m;

    std::cout << "Gamma da costruttore: " << adpGains_.gamma.transpose() << std::endl;
    std::cout << ": " << adpGains_.gamma.transpose() << std::endl;
}

AdaptiveControlVehicle::AdaptiveControlVehicle(ctb::PIDGains pidGains, double sampleTime)
    : DynamicController(sampleTime)
{
    s_.setZero(6);
    R_.setIdentity(3, 3);
    Y_.setZero(6, 9);
    gamma_.setZero(9);
    pose_error_.setZero(6);
    velocity_error_.setZero(6);

    integrale.setZero(6);

    z_.resize(3);
    z_ << 0, 0, 1;

    
    pidGains_.Kp.resize(6);
    pidGains_.Kd.resize(6);
    pidGains_.Ki.resize(6);
    pidGains_.Kff.resize(6);
    pidGains_ = pidGains;

    //std::cout << "\nInizializzato pid\n"<< "Kp: " << pidGains_.Kp.transpose() << "\nKd: " << pidGains_.Kd.transpose() << "\nKi: " << pidGains_.Ki.transpose() << "\nKff: " << pidGains_.Kff.transpose() << std::endl;
}

AdaptiveControlVehicle::~AdaptiveControlVehicle()
{
}

double AdaptiveControlVehicle::ActivationFunction(double in, double thresh1, double thresh2){

    double out;

    if(in < -thresh2){
        out = 0.0;
    }
    else if(in > thresh2){
        out = 0.0;
    }
    else if(abs(in) <= thresh1){
        out = 1.0;
    }
    else{
        out = 0.5*(1+cos(PI*(abs(in)-thresh1)/(thresh2-thresh1)));
    }

    return out;
}

void AdaptiveControlVehicle::ComputeDynamicControl()
{

    switch (type_)
    {

    case 1:
    {
        R_ = myQuat2Rot(fbkQuat_);
        Vector3d fbkRpy;
        fbkRpy = quat2rpy(fbkQuat_);
        velocity_error_ = desVel_ - fbkVel_;

        s_ = desVel_ - fbkVel_;

        printf("\n------------------------------------------------------------------\n");

        printf("\nFeedback_XYZ:      X: %4.2f \t  Y: %4.2f \t  Z: %4.2f \n", fbkPose_(0), fbkPose_(1), fbkPose_(2));
        printf("Feedback_RPY:      Roll: %4.2f \t  Pitch: %4.2f \t  Yaw: %4.2f \n", fbkRpy(0), fbkRpy(1), fbkRpy(2));

        printf("\nFeedback_LinVel:   Vx: %4.2f \t  Vy: %4.2f \t  Vz: %4.2f \n", fbkVel_(0), fbkVel_(1), fbkVel_(2));
        printf("Desired_LinVel:    Vx: %4.2f \t  Vy: %4.2f \t  Vz: %4.2f \n", desVel_(0), desVel_(1), desVel_(2));

        printf("\nFeedback_AngVel:   Wx: %4.2f \t  Wy: %4.2f \t  Wz: %4.2f \n", fbkVel_(3), fbkVel_(4), fbkVel_(5));
        printf("Desired_AngVel:    Wx: %4.2f \t  Wy: %4.2f \t  Wz: %4.2f \n", desVel_(3), desVel_(4), desVel_(5));

        regressor();
        dynamicControl_ = adpGains_.Ks.asDiagonal() * s_ + Y_ * adpGains_.gamma;
        gamma_dot_ = adpGains_.Kg.asDiagonal() * Y_.transpose() * s_;
        for (int i = 0; i < adpGains_.gamma.size(); i++)
        {
            if (adpGains_.en_integration[i])
                adpGains_.gamma(i) = adpGains_.gamma(i) + gamma_dot_(i) * ts_;
        }

        IntegrationSat(lowB_, upB_);
    }
    break;

    case 2:
    {


        R_ = myQuat2Rot(fbkQuat_);
        Vector3d fbkRpy;
        printf("\n------------------------------------------------------------------\n");
        fbkRpy = quat2rpy(fbkQuat_);
        printf("\nFeedback_RPY:      Roll: %4.2f \t  Pitch: %4.2f \t  Yaw: %4.2f \n", fbkRpy(0), fbkRpy(1), fbkRpy(2));
        MatrixXd Tinv(3, 3);

        Tinv << 1, 0, -std::sin(fbkRpy(1)),
            0, std::cos(fbkRpy(0)), std::cos(fbkRpy(1)) * std::sin(fbkRpy(0)),
            0, -std::sin(fbkRpy(0)), std::cos(fbkRpy(1)) * std::cos(fbkRpy(0));

        MatrixXd T(3, 3);

        T << 1, std::cos(fbkRpy(0)) * std::tan(fbkRpy(1)), std::cos(fbkRpy(0)) * std::tan(fbkRpy(1)),
            0, std::cos(fbkRpy(0)), -std::sin(fbkRpy(1)),
            0, std::sin(fbkRpy(0)) / std::cos(fbkRpy(1)), std::cos(fbkRpy(0)) / std::cos(fbkRpy(1));

        pose_error_.head(3) = desPose_.head(3) - fbkPose_.head(3);

        Eigen::VectorXd desQuat = rpy2quat(desPose_.tail(3));

        printf("Desired_RPY:       Roll: %4.2f \t  Pitch: %4.2f \t  Yaw: %4.2f \n", desPose_(3), desPose_(4), desPose_(5));
        printf("\nFeedback_XYZ:      X: %4.2f \t  Y: %4.2f \t  Z: %4.2f \n", fbkPose_(0), fbkPose_(1), fbkPose_(2));
        printf("Desired_XYZ:       X: %4.2f \t  Y: %4.2f \t  Z: %4.2f \n", desPose_(0), desPose_(1), desPose_(2));
        pose_error_.tail(3) = quatError(desQuat, fbkQuat_);

        printf("\nFeedbackQuat:      Ex: %4.2f \t  Ey: %4.2f \t  Ez: %4.2f \t Eta: %4.2f \n", fbkQuat_(0), fbkQuat_(1), fbkQuat_(2), fbkQuat_(3));
        printf("DesiredQuat:       Ex: %4.2f \t  Ey: %4.2f \t  Ez: %4.2f \t Eta: %4.2f \n", desQuat(0), desQuat(1), desQuat(2), desQuat(3));
        printf("QuatError:         Ex: %4.2f \t  Ey: %4.2f \t  Ez: %4.2f \n", quatError(desQuat, fbkQuat_)(0), quatError(desQuat, fbkQuat_)(1), quatError(desQuat, fbkQuat_)(2));
        velocity_error_ = desVel_ - fbkVel_;

        MatrixXd Rt(6, 6);
        Rt.block(0, 0, 3, 3) = R_.transpose();
        Rt.block(0, 3, 3, 3).setZero();
        Rt.block(3, 0, 3, 3).setZero();
        Rt.block(3, 3, 3, 3).setIdentity(); // = Tinv;//R_.transpose();//.setIdentity(); // R_.transpose();

        s_ = (desVel_ - fbkVel_) + adpGains_.Kl.asDiagonal() * (Rt * pose_error_);
        regressor();

        dynamicControl_ = adpGains_.Ks.asDiagonal() * s_ + Y_ * adpGains_.gamma;
        //dynamicControl_ = adpGains_.Ks.asDiagonal() * (desVel_ - fbkVel_) + adpGains_.Kl.asDiagonal() * (Rt * pose_error_) + Y_ * adpGains_.gamma;


        double thresh1 = 0.01, thresh2 = 0.03;
        double act = ActivationFunction(fbkPose_(4), thresh1, thresh2);

        MatrixXd D(9,9);
        D.setIdentity();
        D(0,0) = act;
        D(2,2) = 1-act;

        //gamma_dot_ = D * adpGains_.Kg.asDiagonal() * Y_.transpose() * s_;

        gamma_dot_ = adpGains_.Kg.asDiagonal() * Y_.transpose() * s_;

        for (int i = 0; i < adpGains_.gamma.size(); i++)
        {
            if (adpGains_.en_integration[i])
                adpGains_.gamma(i) = adpGains_.gamma(i) + gamma_dot_(i) * ts_;
        }

        IntegrationSat(lowB_, upB_);
    }
    break;

    case 3:
    {
        R_ = myQuat2Rot(fbkQuat_);
        Vector3d fbkRpy;
        printf("\n------------------------------------------------------------------\n");
        fbkRpy = quat2rpy(fbkQuat_);
        pose_error_.head(3) = desPose_.head(3) - fbkPose_.head(3);
        Eigen::VectorXd desQuat = rpy2quat(desPose_.tail(3));

        printf("Desired_RPY:       Roll: %4.2f \t  Pitch: %4.2f \t  Yaw: %4.2f \n", desPose_(3), desPose_(4), desPose_(5));
        printf("Feedback_RPY:      Roll: %4.2f \t  Pitch: %4.2f \t  Yaw: %4.2f \n", fbkRpy(0), fbkRpy(1), fbkRpy(2));
        printf("Feedback_XYZ:      X: %4.2f \t  Y: %4.2f \t  Z: %4.2f \n", fbkPose_(0), fbkPose_(1), fbkPose_(2));
        printf("Desired_XYZ:       X: %4.2f \t  Y: %4.2f \t  Z: %4.2f \n\n", desPose_(0), desPose_(1), desPose_(2));
        pose_error_.tail(3) = quatError(desQuat, fbkQuat_);
        velocity_error_ = desVel_ - fbkVel_;

        MatrixXd Rt(6, 6);
        Rt.block(0, 0, 3, 3) = R_.transpose();
        Rt.block(0, 3, 3, 3).setZero();
        Rt.block(3, 0, 3, 3).setZero();
        Rt.block(3, 3, 3, 3).setIdentity(); // = Tinv;//R_.transpose();//.setIdentity(); // R_.transpose();


        integrale = integrale + Rt * pose_error_ * ts_;
        dynamicControl_ = pidGains_.Kp.asDiagonal() * Rt * pose_error_ + pidGains_.Kd.asDiagonal() * (desVel_ - fbkVel_) + pidGains_.Ki.asDiagonal() *  integrale + pidGains_.Kff;

        //std::cout << dynamicControl_.transpose() << "\n\n";

    }
    break;
    }
}

Eigen::Matrix3d AdaptiveControlVehicle::mySkew(Eigen::VectorXd v)
{

    Eigen::Matrix3d skew;

    skew << 0, -v(2), v(1), v(2), 0, -v(0), -v(1), v(0), 0;

    return skew;
}

Eigen::Matrix3d AdaptiveControlVehicle::myQuat2Rot(Eigen::VectorXd e)
{
    Eigen::Matrix3d R;
    double t[10];

    t[0] = pow(e(0), 2); //x^2
    t[1] = pow(e(1), 2); //y^2
    t[2] = pow(e(2), 2); //z^2
    t[3] = pow(e(3), 2); //eta^2
    t[4] = e[0] * e[1];  //x*y
    t[5] = e[0] * e[2];  //x*z
    t[6] = e[1] * e[2];  //y*z
    t[7] = e[3] * e[0];  //eta*x
    t[8] = e[3] * e[1];  //eta*y
    t[9] = e[3] * e[2];  //eta*z
    R(0, 0) = 2 * (t[3] + t[0]) - 1;
    R(0, 1) = 2 * (t[4] - t[9]);
    R(0, 2) = 2 * (t[5] + t[8]);

    R(1, 0) = 2 * (t[4] + t[9]);
    R(1, 1) = 2 * (t[3] + t[1]) - 1;
    R(1, 2) = 2 * (t[6] - t[7]);

    R(2, 0) = 2 * (t[5] - t[8]);
    R(2, 1) = 2 * (t[6] + t[7]);
    R(2, 2) = 2 * (t[3] + t[2]) - 1;

    return R;
}

void AdaptiveControlVehicle::IntegrationSat(Eigen::VectorXd lowerB, Eigen::VectorXd upperB)
{

    for (int i = 0; i < adpGains_.gamma.size(); i++)
    {
        if (adpGains_.gamma(i) > upperB(i))
        {
            adpGains_.gamma(i) = upperB(i);
        }
        else if (adpGains_.gamma(i) < lowerB(i))
        {
            adpGains_.gamma(i) = lowerB(i);
        }
        else
        {
            continue;
        }
    }
}

void AdaptiveControlVehicle::SetGains(ctb::ADPGains adpGains)
{
    adpGains_ = adpGains;
}

ctb::ADPGains AdaptiveControlVehicle::GetGains()
{
    return adpGains_;
}

void AdaptiveControlVehicle::regressor()
{
    Y_.block(0, 3, 3, 3) << R_.transpose();
    Y_.block(3, 0, 3, 3) << mySkew(R_.transpose() * z_);
    Y_.block(3, 6, 3, 3) << R_.transpose();
}

void AdaptiveControlVehicle::SetControlType(int type)
{
    type_ = type;
}

Eigen::VectorXd AdaptiveControlVehicle::GetGamma()
{

    return adpGains_.gamma;
}

Eigen::VectorXd AdaptiveControlVehicle::GetPoseError()
{

    return pose_error_;
}

Eigen::VectorXd AdaptiveControlVehicle::GetVelocityError()
{

    return velocity_error_;
}


Eigen::VectorXd AdaptiveControlVehicle::GetIntegralAction(){
    return integrale;
}

Eigen::VectorXd AdaptiveControlVehicle::GetIntegralActionAdaptive(){
    return Y_ * adpGains_.gamma;
}