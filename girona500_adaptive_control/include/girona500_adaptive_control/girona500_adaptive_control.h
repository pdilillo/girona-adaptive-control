#include <eigen3/Eigen/Core>
#include <dynamic_vehicle_control/AdaptiveControlVehicle.h>
#include <dynamic_vehicle_control/aux.h>
#include <ros/ros.h>
#include <cola2_msgs/BodyForceReq.h>
#include <cola2_msgs/BodyVelocityReq.h>
#include <cola2_msgs/GoalDescriptor.h>
#include <cola2_msgs/Bool6Axis.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <cola2_msgs/NavSts.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <dynamic_reconfigure/server.h>
#include <girona500_adaptive_control/AdaptiveGainsConfig.h>
#include <girona500_adaptive_control/DesiredPose.h>
#include <std_msgs/Bool.h>

using namespace std;
using namespace Eigen;

class AdaptiveVehicleROSController
{
private:
  ros::NodeHandle nh_;

  // Name
  std::string name_;

  // Frame id
  std::string frame_id_;

  // Controller frequency
  double frequency_;

  // Publisher
  ros::Publisher force_pub_;
  ros::Publisher params_pub_;
  ros::Publisher error_pub_;
  ros::Publisher integral_pub_;

  // Subscriber
  ros::Subscriber nav_sub_;
  ros::Subscriber odometry_sub_;
  ros::Subscriber reference_pose_sub_;
  ros::Subscriber reference_twist_sub_;
  ros::Subscriber pitch_oscillation_sub_;

  // Timers
  ros::Timer timer_;
  ros::Time start_;
  ros::Time end_;
  int state_;

  // Feedback variables
  VectorXd pose_feedback_;
  VectorXd twist_feedback_;
  VectorXd quat_feedback_;

  VectorXd odom_pos_feedback_;
  VectorXd odom_twist_feedback_;
  VectorXd odom_quat_feedback_;

  // Controller output
  VectorXd ft_command_;
  VectorXd params_;
  VectorXd pose_error_;
  VectorXd velocity_error_;
  VectorXd integral_;

  // Reference variables
  VectorXd desired_pose_;
  VectorXd desired_vel_;

  double last_altitude_;
  double last_altitude_age_;
  double last_depth_;

  // Adaptive controller ptr.
  //std::shared_ptr<AdaptiveControlVehicle> adp_controller_;
  AdaptiveControlVehicle *adp_controller_;
  
  bool reference_pose_init_;
  bool reference_twist_init_;
  bool fbk_init_;
  bool pitch_oscillation_;
  bool enable_pid_;

  dynamic_reconfigure::Server<girona500_adaptive_control::AdaptiveGainsConfig> cfg_server_adp;
  dynamic_reconfigure::Server<girona500_adaptive_control::AdaptiveGainsConfig>::CallbackType f_adp;

public:
  AdaptiveVehicleROSController(const std::string name, const std::string frame_id, double period);
  

  void NavCB(const cola2_msgs::NavStsConstPtr &msg);
  void OdometryCB(const nav_msgs::OdometryConstPtr &msg);
  void ReferencePoseCB(const girona500_adaptive_control::DesiredPoseConstPtr &msg);
  void ReferenceTwistCB(const geometry_msgs::TwistStampedConstPtr &msg);
  void TimerCB(const ros::TimerEvent &event);
  void PublishCommand();
  void PublishParameters();
  void PublishError();
  void PublishIntegralAction();
  void DynamicReconfigureCB(girona500_adaptive_control::AdaptiveGainsConfig &config, uint32_t level);
  void pitchOscillationCB(const std_msgs::BoolConstPtr &msg);
};
