#include <girona500_adaptive_control.h>
#include "ros/ros.h"
#include <cola2_msgs/BodyForceReq.h>
#include <cola2_msgs/BodyVelocityReq.h>
#include <cola2_msgs/GoalDescriptor.h>
#include <cola2_msgs/Bool6Axis.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <cola2_msgs/NavSts.h>
#include <girona500_adaptive_control/Parameters.h>
#include <girona500_adaptive_control/Error.h>
#include <girona500_adaptive_control/DesiredPose.h>

#include <iomanip>
using namespace std;

AdaptiveVehicleROSController::AdaptiveVehicleROSController(const std::string name, const std::string frame_id, double period)
{

  // Variables initialization
  name_ = name;
  frame_id_ = frame_id;
  pose_feedback_.resize(6);
  twist_feedback_.resize(6);
  quat_feedback_.resize(4);
  odom_pos_feedback_.resize(3);
  odom_twist_feedback_.resize(6);
  odom_quat_feedback_.resize(4);

  desired_pose_.resize(6);
  frequency_ = 1 / period;
  cout << "frequency: " << frequency_ << endl;
  desired_vel_.resize(6);
  desired_vel_.setZero();
  ft_command_.resize(6);
  pose_error_.resize(6);
  velocity_error_.resize(6);

  fbk_init_ = false;
  reference_pose_init_ = false;
  reference_twist_init_ = false;
  pitch_oscillation_ = false;

  // Publishers
  force_pub_ = nh_.advertise<cola2_msgs::BodyForceReq>("/girona500/controller/body_force_req", 1);
  params_pub_ = nh_.advertise<girona500_adaptive_control::Parameters>("/girona500/adp_controller/parameters", 1);
  error_pub_ = nh_.advertise<girona500_adaptive_control::Error>("/girona500/adp_controller/error", 1);
  integral_pub_ = nh_.advertise<girona500_adaptive_control::Error>("/girona500/adp_controller/integral_action", 1);

  // Subscribers
  nav_sub_ = nh_.subscribe<cola2_msgs::NavSts>("/girona500/navigator/navigation", 1, &AdaptiveVehicleROSController::NavCB, this);
  reference_pose_sub_ = nh_.subscribe<girona500_adaptive_control::DesiredPose>("/girona500/adp_controller/pose_reference", 1, &AdaptiveVehicleROSController::ReferencePoseCB, this);
  reference_twist_sub_ = nh_.subscribe<geometry_msgs::TwistStamped>("/girona500/adp_controller/twist_reference", 1, &AdaptiveVehicleROSController::ReferenceTwistCB, this);
  odometry_sub_ = nh_.subscribe<nav_msgs::Odometry>("/girona500/navigator/odometry", 1, &AdaptiveVehicleROSController::OdometryCB, this);
  pitch_oscillation_sub_ = nh_.subscribe<std_msgs::Bool>("/girona500/pitch_oscillation", 1, &AdaptiveVehicleROSController::pitchOscillationCB, this);

  timer_ = nh_.createTimer(ros::Duration(period), &AdaptiveVehicleROSController::TimerCB, this);

  // Gains
  ctb::ADPGains adpGains_;
  adpGains_.Kl.resize(6);
  adpGains_.Kg.resize(9);
  adpGains_.Ks.resize(6);
  adpGains_.gamma.resize(9);

  adpGains_.Kl << 1, 1, 1, 1, 1, 1;                   //!< Pose error Proportional gain
  adpGains_.Kg << 25, 25, 25, 25, 25, 25, 25, 25, 25; //!< Dynamic parameters update law gain
  adpGains_.Ks << 150, 150, 150, 10, 10, 60;          //!< Velocity error Proportional gain
  adpGains_.gamma << -1.57, 0, 0, 0, 0, -14, 0, 0, 0;
  adpGains_.gamma << -4.85, 0, 144.28, 0, 0, 18.81, 0, 0, 0;
  adpGains_.upGB_m << -1, 0, 200;
  adpGains_.lowGB_m << -6, 0, 50;
  adpGains_.up_f << 500, 500, 500;
  adpGains_.low_f << -500, -500, -500;
  adpGains_.up_m << 100, 100, 50;
  adpGains_.low_m << -100, -100, -50;

  double kp_x, kp_y, kp_z, kp_roll, kp_pitch, kp_yaw;
  double kd_x, kd_y, kd_z, kd_roll, kd_pitch, kd_yaw;
  double ki_x, ki_y, ki_z, ki_roll, ki_pitch, ki_yaw;
  double kff_x, kff_y, kff_z, kff_roll, kff_pitch, kff_yaw;

  ctb::PIDGains pidGains_;
  nh_.getParam("/kp_x", kp_x);
  nh_.getParam("/kp_y", kp_y);
  nh_.getParam("/kp_z", kp_z);
  nh_.getParam("/kp_roll", kp_roll);
  nh_.getParam("/kp_pitch", kp_pitch);
  nh_.getParam("/kp_yaw", kp_yaw);
  nh_.getParam("/kd_x", kd_x);
  nh_.getParam("/kd_y", kd_y);
  nh_.getParam("/kd_z", kd_z);
  nh_.getParam("/kd_roll", kd_roll);
  nh_.getParam("/kd_pitch", kd_pitch);
  nh_.getParam("/kd_yaw", kd_yaw);
  nh_.getParam("/ki_x", ki_x);
  nh_.getParam("/ki_y", ki_y);
  nh_.getParam("/ki_z", ki_z);
  nh_.getParam("/ki_roll", ki_roll);
  nh_.getParam("/ki_pitch", ki_pitch);
  nh_.getParam("/ki_yaw", ki_yaw);
  nh_.getParam("/ff_x", kff_x);
  nh_.getParam("/ff_y", kff_y);
  nh_.getParam("/ff_z", kff_z);
  nh_.getParam("/ff_roll", kff_roll);
  nh_.getParam("/ff_pitch", kff_pitch);
  nh_.getParam("/ff_yaw", kff_yaw);

  pidGains_.Kp.resize(6);
  pidGains_.Kd.resize(6);
  pidGains_.Ki.resize(6);
  pidGains_.Kff.resize(6);

  pidGains_.Kp << kp_x, kp_y, kp_z, kp_roll, kp_pitch, kp_yaw;
  pidGains_.Kd << kd_x, kd_y, kd_z, kd_roll, kd_pitch, kd_yaw;
  pidGains_.Ki << ki_x, ki_y, ki_z, ki_roll, ki_pitch, ki_yaw;
  pidGains_.Kff << kff_x, kff_y, kff_z, kff_roll, kff_pitch, kff_yaw;

  nh_.getParam("/enable_pid", enable_pid_);

  // Controller
  if (enable_pid_)
  {
    adp_controller_ = new AdaptiveControlVehicle(pidGains_, period);
    adp_controller_->SetControlType(3);
    cout << "PID controller initialized\n\n";
  }
  else
  {
    adp_controller_ = new AdaptiveControlVehicle(adpGains_, period);
    adp_controller_->SetControlType(2);
    // Dynamic reconfigure
    f_adp = boost::bind(&AdaptiveVehicleROSController::DynamicReconfigureCB, this, _1, _2);
    cfg_server_adp.setCallback(f_adp);
    cout << "Adaptive controller initialized\n\n";
  }

  start_ = ros::Time::now();
  state_ = 0;
}

void AdaptiveVehicleROSController::ReferencePoseCB(const girona500_adaptive_control::DesiredPoseConstPtr &msg)
{

  desired_pose_(0) = msg->position.x;
  desired_pose_(1) = msg->position.y;
  desired_pose_(2) = msg->position.z;

  desired_pose_(3) = msg->rpy.x;
  desired_pose_(4) = msg->rpy.y;
  desired_pose_(5) = msg->rpy.z;

  reference_pose_init_ = true;
}

void AdaptiveVehicleROSController::ReferenceTwistCB(const geometry_msgs::TwistStampedConstPtr &msg)
{

  desired_vel_(0) = msg->twist.linear.x;
  desired_vel_(1) = msg->twist.linear.y;
  desired_vel_(2) = msg->twist.linear.z;
  desired_vel_(3) = msg->twist.angular.x;
  desired_vel_(4) = msg->twist.angular.y;
  desired_vel_(5) = msg->twist.angular.z;

  reference_twist_init_ = true;
}

void AdaptiveVehicleROSController::NavCB(const cola2_msgs::NavStsConstPtr &msg)
{

  // Update pose feedback
  pose_feedback_(0) = msg->position.north;
  pose_feedback_(1) = msg->position.east;
  pose_feedback_(2) = msg->position.depth;
  pose_feedback_(3) = msg->orientation.roll;
  pose_feedback_(4) = msg->orientation.pitch;
  pose_feedback_(5) = msg->orientation.yaw;
  quat_feedback_ = rpy2quat(pose_feedback_.tail(3));

  // Update twist feedback
  twist_feedback_(0) = msg->body_velocity.x;
  twist_feedback_(1) = msg->body_velocity.y;
  twist_feedback_(2) = msg->body_velocity.z;

  MatrixXd Tinv(3, 3);

  Tinv << 1, 0, -std::sin(pose_feedback_(4)),
      0, std::cos(pose_feedback_(3)), std::cos(pose_feedback_(4)) * std::sin(pose_feedback_(3)),
      0, -std::sin(pose_feedback_(3)), std::cos(pose_feedback_(4)) * std::cos(pose_feedback_(3));

  Vector3d rpy_rate;
  rpy_rate << msg->orientation_rate.roll, msg->orientation_rate.pitch, msg->orientation_rate.yaw;
  twist_feedback_.tail(3) = Tinv * rpy_rate;
  //cout << "Twist feedback: " << twist_feedback_.tail(3).transpose() << endl;
  //cout << "RPY rate: " << rpy_rate.transpose() << endl << endl;
  /*
  twist_feedback_(3) = msg->orientation_rate.roll;
  twist_feedback_(4) = msg->orientation_rate.pitch;
  twist_feedback_(5) = msg->orientation_rate.yaw;
  */
  // Stores last altitude. If altitude is invalid, during 5 seconds estimate it wrt last altitude and delta depth.
  // If more than 5 seconds put it a 0.5.
  if (msg->altitude > 0.0)
  {
    last_altitude_ = msg->altitude;
    last_altitude_age_ = msg->header.stamp.toSec();
    last_depth_ = msg->position.depth;
  }
  else
  {
    if ((ros::Time::now().toSec() - last_altitude_age_) > 5.0)
    {
      last_altitude_ = 0.5;
    }
    else
    {
      last_altitude_ = last_altitude_ - (msg->position.depth - last_depth_);
      last_depth_ = msg->position.depth;
    }
  }

  fbk_init_ = true;
}

void AdaptiveVehicleROSController::OdometryCB(const nav_msgs::OdometryConstPtr &msg)
{

  odom_pos_feedback_(0) = msg->pose.pose.position.x;
  odom_pos_feedback_(1) = msg->pose.pose.position.y;
  odom_pos_feedback_(2) = msg->pose.pose.position.z;

  odom_quat_feedback_(0) = msg->pose.pose.orientation.x;
  odom_quat_feedback_(1) = msg->pose.pose.orientation.y;
  odom_quat_feedback_(2) = msg->pose.pose.orientation.z;
  odom_quat_feedback_(3) = msg->pose.pose.orientation.w;

  odom_twist_feedback_(0) = msg->twist.twist.linear.x;
  odom_twist_feedback_(1) = msg->twist.twist.linear.y;
  odom_twist_feedback_(2) = msg->twist.twist.linear.z;
  odom_twist_feedback_(3) = msg->twist.twist.angular.x;
  odom_twist_feedback_(4) = msg->twist.twist.angular.y;
  odom_twist_feedback_(5) = msg->twist.twist.angular.z;

  //twist_feedback_ = odom_twist_feedback_;
  //cout << "\nodom_pos_feedback_:" << odom_pos_feedback_.transpose()<< endl;

  fbk_init_ = true;
}

void AdaptiveVehicleROSController::PublishCommand()
{

  cola2_msgs::BodyForceReq ReqMsg;
  cola2_msgs::GoalDescriptor GoalMsg;
  geometry_msgs::Wrench WrenchMsg;
  cola2_msgs::Bool6Axis BoolMsg;

  GoalMsg.requester = name_;
  GoalMsg.priority = 10;

  WrenchMsg.force.x = ft_command_(0);
  WrenchMsg.force.y = ft_command_(1);
  WrenchMsg.force.z = ft_command_(2);
  WrenchMsg.torque.x = ft_command_(3);
  WrenchMsg.torque.y = ft_command_(4);
  WrenchMsg.torque.z = ft_command_(5);

  BoolMsg.x = false;
  BoolMsg.y = false;
  BoolMsg.z = false;
  BoolMsg.roll = true;
  BoolMsg.pitch = false;
  BoolMsg.yaw = false;

  ReqMsg.header.frame_id = "/girona500/" + frame_id_;
  ReqMsg.goal = GoalMsg;
  ReqMsg.wrench = WrenchMsg;
  ReqMsg.disable_axis = BoolMsg;

  ReqMsg.header.stamp = ros::Time::now();

  force_pub_.publish(ReqMsg);
}

void AdaptiveVehicleROSController::TimerCB(const ros::TimerEvent &event)
{

  if (reference_twist_init_ && fbk_init_ && !reference_pose_init_)
  {

    // Using navigation filter
    adp_controller_->ReadPoseFeedback(pose_feedback_);
    adp_controller_->ReadFeedback(twist_feedback_);
    adp_controller_->SetDesiredVel(desired_vel_);
    adp_controller_->GetVehicleOri(quat_feedback_);

    // Using odometry
    /*adp_controller_->ReadFeedback(odom_twist_feedback_);
    adp_controller_->SetDesiredVel(desired_vel_);
    adp_controller_->GetVehicleOri(odom_quat_feedback_);
   */
    adp_controller_->SetControlType(1);
    adp_controller_->ComputeDynamicControl();
    ft_command_ = adp_controller_->GetDynamicControl();
    /*if (state_ == 0 && pitch_oscillation_)
      ft_command_(4) = 50;
    if (state_ == 1 && pitch_oscillation_)
      ft_command_(4) = -50;
    end_ = ros::Time::now();
    if (end_ - start_ > ros::Duration(20.0))
    {
      if (state_ == 0)
        state_ = 1;
      else if (state_ == 1)
        state_ = 0;
      start_ = end_;
    }
*/
    printf("\nControl Forces:    Fx: %4.2f \t  Fy: %4.2f \t  Fz: %4.2f \n", ft_command_(0), ft_command_(1), ft_command_(2));
    printf("Control Torques:   Tx: %4.2f \t  Ty: %4.2f \t  Tz: %4.2f \n", ft_command_(3), ft_command_(4), ft_command_(5));

    PublishCommand();
    PublishParameters();
    PublishError();
  }

  else if (reference_pose_init_ && fbk_init_)
  {
    // Using navigation filter

    adp_controller_->ReadFeedback(twist_feedback_);
    adp_controller_->ReadPoseFeedback(pose_feedback_);
    adp_controller_->SetDesiredVel(desired_vel_);
    adp_controller_->SetDesiredPose(desired_pose_);
    adp_controller_->GetVehicleOri(quat_feedback_);

    // Using odometry
    /*
    adp_controller_->ReadFeedback(odom_twist_feedback_);
    adp_controller_->ReadPoseFeedback(odom_pos_feedback_);
    adp_controller_->SetDesiredVel(desired_vel_);
    adp_controller_->SetDesiredPose(desired_pose_);
    adp_controller_->GetVehicleOri(odom_quat_feedback_);
  */

    //adp_controller_->SetControlType(3);
    adp_controller_->ComputeDynamicControl();
    ft_command_ = adp_controller_->GetDynamicControl();

    if (enable_pid_)
    {

      integral_ = adp_controller_->GetIntegralAction();
      PublishIntegralAction();
      //  PublishError();
    }

    /*
    end_ = ros::Time::now();
    if (end_ - start_ > ros::Duration(3.0))
    {
      if (state_ == 0)
        state_ = 1;
      else if (state_ == 1)
        state_ = 0;
      start_ = end_;
    }
*/

    printf("\nControl Forces:    Fx: %4.2f \t  Fy: %4.2f \t  Fz: %4.2f \n", ft_command_(0), ft_command_(1), ft_command_(2));
    printf("Control Torques:   Tx: %4.2f \t  Ty: %4.2f \t  Tz: %4.2f \n", ft_command_(3), ft_command_(4), ft_command_(5));
    PublishCommand();

    if (!enable_pid_){
      PublishParameters();
      integral_ = adp_controller_->GetIntegralActionAdaptive();
    PublishIntegralAction();
    }
    PublishError();
    
  }
}

void AdaptiveVehicleROSController::PublishParameters()
{

  params_ = adp_controller_->GetGamma();
  girona500_adaptive_control::Parameters msg;
  msg.header.stamp = ros::Time::now();
  msg.array.data.resize(params_.size());

  for (int i = 0; i < params_.size(); i++)
  {
    msg.array.data[i] = params_(i);
  }
  printf("\nDynamic Params:    GBx: %4.2f \t  GBy: %4.2f \t  GBz: %4.2f \n ", params_(0), params_(1), params_(2));
  //printf("g1_GBx: %4.2f \t g2_GBy: %4.2f \t g3_GBz: %4.2f \n ", params_(0), params_(1), params_(2));
  printf("\t \t   Fx: %4.2f \t  Fy: %4.2f \t  Fz: %4.2f \n ", params_(3), params_(4), params_(5));
  printf("\t \t   Tx: %4.2f \t  Ty: %4.2f \t  Tz: %4.2f \n ", params_(6), params_(7), params_(8));
  params_pub_.publish(msg);
}

void AdaptiveVehicleROSController::PublishIntegralAction()
{
  girona500_adaptive_control::Error IntegralMsg;
  IntegralMsg.pose_error.data.resize(6);
  for (int i = 0; i < 6; i++)
  {
    IntegralMsg.pose_error.data[i] = integral_(i);
  }

  integral_pub_.publish(IntegralMsg);
}

void AdaptiveVehicleROSController::PublishError()
{

  pose_error_ = adp_controller_->GetPoseError();
  velocity_error_ = adp_controller_->GetVelocityError();
  girona500_adaptive_control::Error ErrorMsg;
  ErrorMsg.header.stamp = ros::Time::now();
  ErrorMsg.pose_error.data.resize(6);
  ErrorMsg.velocity_error.data.resize(6);

  for (int i = 0; i < 6; i++)
  {
    ErrorMsg.pose_error.data[i] = pose_error_(i);
    ErrorMsg.velocity_error.data[i] = velocity_error_(i);
  }

  error_pub_.publish(ErrorMsg);
}

void AdaptiveVehicleROSController::DynamicReconfigureCB(girona500_adaptive_control::AdaptiveGainsConfig &config, uint32_t level)
{

  ctb::ADPGains outADP = adp_controller_->GetGains();
  outADP.Kl.resize(6);
  outADP.Ks.resize(6);
  outADP.Kg.resize(9);
  outADP.en_integration.resize(9);

  Eigen::VectorXd Kl(6);
  Eigen::VectorXd Kg(9);
  Eigen::VectorXd Ks(6);
  Eigen::VectorXd gamma(9);
  gamma = adp_controller_->GetGamma();

  vector<bool> integration = {true, true, true, true, true, true, true, true, true};

  //localconfig = config;

  Kl(0) = config.Kl_x;
  Kl(1) = config.Kl_y;
  Kl(2) = config.Kl_z;
  Kl(3) = config.Kl_roll;
  Kl(4) = config.Kl_pitch;
  Kl(5) = config.Kl_yaw;

  Ks(0) = config.Ks_Vx;
  Ks(1) = config.Ks_Vy;
  Ks(2) = config.Ks_Vz;
  Ks(3) = config.Ks_Wx;
  Ks(4) = config.Ks_Wy;
  Ks(5) = config.Ks_Wz;

  Kg(0) = config.Kg1_GBx;
  Kg(1) = config.Kg2_GBy;
  Kg(2) = config.Kg3_GBz;
  Kg(3) = config.Kg4_Fx;
  Kg(4) = config.Kg5_Fy;
  Kg(5) = config.Kg6_Fz;
  Kg(6) = config.Kg7_Tx;
  Kg(7) = config.Kg8_Ty;
  Kg(8) = config.Kg9_Tz;

  if (config.Reset_Stop_g1_GBx)
  {
    gamma(0) = config.g1_GBx;
    integration[0] = false;
  }
  else
  {
    integration[0] = true;
  }

  if (config.Reset_Stop_g2_GBy)
  {
    gamma(1) = config.g2_GBy;
    integration[1] = false;
  }
  else
  {
    integration[1] = true;
  }

  if (config.Reset_Stop_g3_GBz)
  {
    gamma(2) = config.g3_GBz;
    integration[2] = false;
  }
  else
  {
    integration[2] = true;
  }

  if (config.Reset_Stop_g4_Fx)
  {
    gamma(3) = config.g4_Fx;
    integration[3] = false;
  }
  else
  {
    integration[3] = true;
  }

  if (config.Reset_Stop_g5_Fy)
  {
    gamma(4) = config.g5_Fy;
    integration[4] = false;
  }
  else
  {
    integration[4] = true;
  }

  if (config.Reset_Stop_g6_Fz)
  {
    gamma(5) = config.g6_Fz;
    integration[5] = false;
  }
  else
  {
    integration[5] = true;
  }

  if (config.Reset_Stop_g7_Tx)
  {
    gamma(6) = config.g7_Tx;
    integration[6] = false;
  }
  else
  {
    integration[6] = true;
  }

  if (config.Reset_Stop_g8_Ty)
  {
    gamma(7) = config.g8_Ty;
    integration[7] = false;
  }
  else
  {
    integration[7] = true;
  }

  if (config.Reset_Stop_g9_Tz)
  {
    gamma(8) = config.g9_Tz;
    integration[8] = false;
  }
  else
  {
    integration[8] = true;
  }

  for (int i = 0; i < 6; i++)
  {
    outADP.Kl(i) = Kl(i);
    outADP.Ks(i) = Ks(i);
  }

  for (int i = 0; i < 9; i++)
  {
    outADP.Kg(i) = Kg(i);
    outADP.gamma(i) = gamma(i);
    outADP.en_integration[i] = integration[i];
  }
  /*cout << Kl.transpose() << "\n"
       << Ks.transpose() << "\n"
       << Kg.transpose() << "\n\n\n";
       */
  adp_controller_->SetGains(outADP);

  ROS_INFO("[girona500_adaptive_control] loaded new ADP dynamic parameters from rqt config");
}

void AdaptiveVehicleROSController::pitchOscillationCB(const std_msgs::BoolConstPtr &msg)
{
  pitch_oscillation_ = msg->data;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "g500_adaptive_control");

  double period = 0.1;

  AdaptiveVehicleROSController _ros_controller("g500_adaptive_control",
                                               "base_link", period);

  ros::spin();

  return 0;
}
